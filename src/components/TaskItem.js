import React from 'react';
import {Card, CardBody, CardHeader} from "reactstrap";

const TaskItem = ({item, setTarget}) => {
    return (
        <Card className='mb-3'>
            <CardHeader className='d-flex justify-content-between align-items-center'>
                <div>{item.title}</div>
                <div onClick={(event) => setTarget(event.target, item)} className='three-dots'>&#9776;</div>
            </CardHeader>
            <CardBody>
                {item.description}
            </CardBody>
        </Card>
    );
}

export default TaskItem;
