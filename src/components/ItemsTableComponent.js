import React, {Component} from "react";
import {Status} from "../data/items";
import '../css/itemCard.css';
import TaskItem from "./TaskItem";
import {Menu, MenuItem} from "@mui/material";


const RenderMenu = ({targetEl, open, closeMenu, deleteItem, item, changeStatus}) => {
    if (!open) return null;
    const specificMenuItems = [];
    switch (item.status) {
        case Status.TO_DO:
            specificMenuItems.push(
                <MenuItem key={'move to in progress'} onClick={() => {closeMenu(); changeStatus(item,Status.IN_PROGRESS);}}>Move to {Status.IN_PROGRESS}</MenuItem>
            );
            break;
        case Status.IN_PROGRESS:
            specificMenuItems.push(
                <MenuItem key={'move to to do'} onClick={() => {closeMenu(); changeStatus(item,Status.TO_DO);}}>Move to {Status.TO_DO}</MenuItem>,
                <MenuItem key={'move to done'} onClick={() => {closeMenu(); changeStatus(item,Status.DONE);}}>Move to {Status.DONE}</MenuItem>
            );
            break;
        case Status.DONE:
            specificMenuItems.push(
                <MenuItem key={'move to in progress'} onClick={() => {closeMenu(); changeStatus(item,Status.IN_PROGRESS);}}>Move to {Status.IN_PROGRESS}</MenuItem>
            );
            break;
        default:
            break;
    }
    return (
        <Menu
            anchorEl={targetEl}
            open={open}
            onClose={closeMenu}
        >
            {specificMenuItems}
            <MenuItem key={'delete'} onClick={() => {closeMenu();deleteItem(item);}}>Delete</MenuItem>
        </Menu>
    );
}

class ItemsTable extends Component {

    constructor(props) {
        super(props);
        this.state = {
            targetEl: null,
            chosenItem: null,
            openMenu: false
        }
    }

    setTarget = (targetEL, item) => {
        this.setState({
            targetEl: targetEL,
            chosenItem: item,
            openMenu: true
        });
    }

    closeMenu = () => {
        this.setState({
            openMenu: false
        });
    }


    render() {
        const toDo = [];
        const inProgress = [];
        const done = [];
        const putInArray = (item) => {
            switch (item.status) {
                case Status.TO_DO:
                    toDo.push(item);
                    break;
                case Status.IN_PROGRESS:
                    inProgress.push(item);
                    break;
                case Status.DONE:
                    done.push(item);
                    break;
                default:
                    break;
            }
        }

        console.log('error here: ', this.props.items);
        this.props.items.forEach(i => putInArray(i));

        const putInCard = (item) => {
            return (
                <TaskItem key={item.id} setTarget={this.setTarget} item={item}/>
            );
        }

        const toDoCards = toDo.map(i => putInCard(i));
        const inProgressCards = inProgress.map(i => putInCard(i));
        const doneCards = done.map(i => putInCard(i));

        return (
            <div className='row'>
                <div className='col-md-4 col-12'>
                    <h2 className='text-center'>To Do</h2>
                    {toDoCards}
                </div>
                <div className='col-md-4 col-12'>
                    <h2 className='text-center'>In Progress</h2>
                    {inProgressCards}
                </div>
                <div className='col-md-4 col-12'>
                    <h2 className='text-center'>Done</h2>
                    {doneCards}
                </div>
                <RenderMenu changeStatus={this.props.changeStatus} deleteItem={this.props.deleteItem} closeMenu={this.closeMenu} open={this.state.openMenu} targetEl={this.state.targetEl} item={this.state.chosenItem}/>
            </div>
        );
    }
}


export default ItemsTable;
