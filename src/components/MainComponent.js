import React, {Component} from 'react';
import AddItemComponent from "./AddItemComponent";
import ItemsTable from "./ItemsTableComponent";
import store from 'store';
import {ITEMS} from "../data/items";


class Main extends Component {

    constructor(props) {
        super(props);
        this.state = {
            items: []
        }
    }

    componentDidMount() {
        this.setState({
            items: store.get('items')
        });
        // store.set('items', ITEMS);
        // store.remove('items');
        console.log(store.get('items'));
    }

    addNewItem = (item) => {
        this.state.items.push(item);
        this.setState({
            items: this.state.items
        }, () => {
            console.log('array after item added: ',this.state.items);
            store.set('items', this.state.items);
        });
    }

    changeStatus = (item, newStatus) => {
        item.status = newStatus;
        store.set('items', this.state.items);
    }

    deleteItem = (item) => {
        console.log('deleting item', item);
        this.setState({
            items: this.state.items.filter(i => i.id !== item.id)
        }, () => {
            store.set('items', this.state.items);
        });
    }


    render() {
        return (
            <div className='container mt-5'>
                <AddItemComponent addNewItem={this.addNewItem}/>
                <ItemsTable changeStatus={this.changeStatus} deleteItem={this.deleteItem} items={this.state.items}/>
            </div>
        );
    }
}


export default Main;
