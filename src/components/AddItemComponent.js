import React, {Component} from 'react';
import {Button, FormGroup, Modal, ModalBody, ModalHeader} from "reactstrap";
import {Status} from "../data/items";


class AddItemComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            openModal: false,
            title: '',
            description: ''
        };
    }

    toggleModal = () => {
        this.setState({
            openModal: !this.state.openModal
        });
        this.resetForm();
    }

    handleOnChange = (event) => {
        const target = event.target;
        this.setState({
            [target.id]: target.value
        });
    }

    addNewItem = (event) => {
        event.preventDefault();
        this.toggleModal();
        const item = {
            id: Math.round(Date.now()*Math.random()),
            title: this.state.title,
            description: this.state.description,
            status: Status.TO_DO
        };
        this.props.addNewItem(item);
    }

    resetForm = () => {
        this.setState({
            title: '',
            description: ''
        });
    }


    render() {
        return (
            <div className='row mb-3 mt-3'>
                <div className='d-flex col-12 justify-content-end'>
                    <Button className='btn btn-outline-secondary' onClick={this.toggleModal}>+ Add New Item</Button>
                </div>
                <Modal isOpen={this.state.openModal} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>
                        New Item
                    </ModalHeader>
                    <ModalBody>
                        <form onSubmit={this.addNewItem}>
                            <FormGroup>
                                <label htmlFor='title'>Title</label>
                                <input onChange={this.handleOnChange} className='form-control' required={true} maxLength={50} type='text' value={this.state.title} id='title'/>
                            </FormGroup>
                            <FormGroup>
                                <label htmlFor='description'>Description</label>
                                <textarea onChange={this.handleOnChange} maxLength={500} rows={4} className='form-control' value={this.state.description} id='description'/>
                            </FormGroup>
                            <FormGroup className='d-flex justify-content-end'>
                                <Button type='reset' onClick={this.toggleModal}>Cancel</Button>
                                <button className='btn btn-primary ml-3' type='submit'>Save</button>
                            </FormGroup>
                        </form>
                    </ModalBody>
                </Modal>
            </div>
        );
    }


}

export default AddItemComponent;
