export const Status = {TO_DO: 'To Do', IN_PROGRESS: 'In Progress', DONE: 'Done'};

export const ITEMS = [
    {
        id: 0,
        title: 'Task Title 1',
        description: 'Finish task 1 by Saturday',
        status: Status.TO_DO
    },
    {
        id: 1,
        title: 'Task Title 2',
        description: 'Finish task 2 by Friday',
        status: Status.DONE
    },
    {
        id: 2,
        title: 'Task Title 3',
        description: 'Finish task 3 by Monday',
        status: Status.IN_PROGRESS
    },
    {
        id: 3,
        title: 'Task Title 4',
        description: 'Finish task 4 by Wednesday',
        status: Status.DONE
    },
    {
        id: 4,
        title: 'Task Title 5',
        description: 'Finish task 5 by Sunday',
        status: Status.DONE
    }
];
